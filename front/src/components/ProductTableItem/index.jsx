import React from "react"
import PropTypes from "prop-types"

import { modalOperations } from "../../store/modal"
import { cartOperations } from "../../store/cart"
import { useDispatch } from "react-redux"

import Button from "../Button"

function ProductTableItem({ product }) {
    const { articul, name, imagePath, price, quantity } = product

    const dispatch = useDispatch()
    const setModalContent = (content) =>
        dispatch(modalOperations.setModalContent(content))
    const manageCart = (content) => dispatch(cartOperations.toggleCart(content))

    return (
        <tr>
            <td>
                <img width="200" src={imagePath} alt={name} />
            </td>
            <td>{name}</td>
            <td>{price}$</td>
            <td>{quantity}</td>
            <td>{price * quantity}$</td>
            <td>
                <button
                    className="product-delete-btn"
                    title="delete from cart"
                    onClick={() => {
                        setModalContent({
                            title: <h2>Warning!</h2>,
                            main: (
                                <h2 style={{ textAlign: "center" }}>
                                    Are you shure you want to remove product
                                    from cart?
                                </h2>
                            ),
                            footer: (
                                <div style={{ textAlign: "center" }}>
                                    <Button
                                        className={"btn-danger"}
                                        func={() => {
                                            manageCart({
                                                articul,
                                                price,
                                                quantity: 1,
                                            })
                                            setModalContent(null)
                                        }}
                                        text="Remove"
                                    />
                                    <Button
                                        func={() => setModalContent(null)}
                                        text={"Cancel"}
                                    />
                                </div>
                            ),
                        })
                    }}
                >
                    x
                </button>
            </td>
        </tr>
    )
}

ProductTableItem.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        articul: PropTypes.string.isRequired,
        imagePath: PropTypes.string.isRequired,
        price: PropTypes.number,
        quantity: PropTypes.number.isRequired,
    }).isRequired,
}

export default ProductTableItem
