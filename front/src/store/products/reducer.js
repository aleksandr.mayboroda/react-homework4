import types from './types'

const initialState = {
    data: [],
    favorites: JSON.parse(localStorage.getItem("favorites")) || [],
    error: null
}

const reducer = (state = initialState, action) =>
{
    switch (action.type)
    {
        case types.SET_PRODUCTS: {
            return {...state, data: action.payload, error: null}
        }
        case types.SET_ERROR: {
            return {...state, error: action.payload}
        }
        case types.TOGGLE_FAVORITES: {
            const articul = action.payload
            if(state.favorites.includes(articul))
            {
                const fav = state.favorites.filter(art => art !== articul)
                localStorage.setItem("favorites", JSON.stringify(fav))
                return {...state, favorites: fav}
            }
            else
            {
                const fav = [...state.favorites, articul]
                localStorage.setItem("favorites", JSON.stringify(fav))
                return {...state, favorites: fav}
            }
            
        }
        default:
            return state
    }
}

export default reducer