import {combineReducers} from 'redux'

import modal from './modal'
import products from './products'
import cart from './cart'

const reducers = combineReducers({
    modal,
    products,
    cart
})

export default reducers