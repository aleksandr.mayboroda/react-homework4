import types from "./types"

const initialState = {
    data: JSON.parse(localStorage.getItem("cart")) || [],
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.TOGGLE_CART: {
            const productInfo = action.payload
            if (state.data.find((itm) => itm.articul === productInfo.articul)) {
                const products = state.data.filter(
                    (product) => product.articul !== productInfo.articul
                )
                localStorage.setItem("cart", JSON.stringify([...products]))
                return { ...state, data: products }
            } else {
                const products = [...state.data, productInfo]
                localStorage.setItem("cart", JSON.stringify(products))
                return { ...state, data: products }
            }
        }
        default:
            return state
    }
}

export default reducer
