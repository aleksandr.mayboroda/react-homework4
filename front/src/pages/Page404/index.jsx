import React from 'react'
import './style.scss'

function Page404() {
    return (
        <div className="page404">
            <h1 className="page404__title">Error</h1>
            <h2 className="page404__subtitle">404</h2>
            <h3 className="page404__description">page not found</h3>
            <img src="https://images.template.net/wp-content/uploads/2016/05/18104455/funny-404-pages.jpg" alt="page_not_found" />
        </div>
    )
}

export default Page404
