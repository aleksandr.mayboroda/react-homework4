import React from "react"
import "./style.scss"

import { modalOperations } from "../../store/modal"
import { useDispatch, useSelector } from "react-redux"

import { productsSelectors, productsOperations } from "../../store/products"
import { cartSelectors, cartOperations } from "../../store/cart"

import PropTypes from "prop-types"
import Button from "../Button"
import Favorite from "../Favorite"

const ProductCard = ({ product }) => {
    const isInFavorites = useSelector(
        productsSelectors.isProductInFavorites(product.articul)
    )

    const dispatch = useDispatch()
    const setModalContent = (content) =>
        dispatch(modalOperations.setModalContent(content))

    const isInCart = useSelector(cartSelectors.isProductInCart(product.articul))
    const cartHandler = (content) =>
        dispatch(cartOperations.toggleCart(content))

    const { name, price = 0, imagePath, articul, color } = product

    return (
        // onMouseEnter={() => console.log('hovered')}
        <div className="product">
            <div className="product__image">
                <img src={imagePath} alt={name} />
            </div>
            <div className="product__info">
                <h3 className="product__name">{name}</h3>

                <p className="product__articul">articul: {articul}</p>
                <p className="product__price">{price} $</p>
                <p className="product__color">
                    color: <span style={{ color }}>{color}</span>
                </p>
                <div className="product__actions">
                    <Favorite
                        func={() =>
                            dispatch(
                                productsOperations.toggleFavorites(articul)
                            )
                        }
                        filled={isInFavorites}
                    />
                    <Button
                        text={isInCart ? "In cart" : "Add to cart"}
                        className={isInCart ? "btn-yellow" : "btn-water"}
                        func={() =>
                            setModalContent({
                                // classes: (isInCart ? 'text-white' : ''),
                                // background: isInCart ? "red" : "",
                                title: <h2>{isInCart ? "Warning!" : ""}</h2>,
                                main: (
                                    <h2 style={{ textAlign: "center" }}>
                                        Are you shure you want
                                        {isInCart
                                            ? `to remove product from `
                                            : ` to add product to `}
                                        cart?
                                    </h2>
                                ),
                                footer: (
                                    <div style={{ textAlign: "center" }}>
                                        <Button
                                            className={
                                                isInCart
                                                    ? "btn-danger"
                                                    : "btn-success"
                                            }
                                            func={() => {
                                                cartHandler({
                                                    articul,
                                                    price,
                                                    quantity: 1,
                                                })
                                                setModalContent(null)
                                            }}
                                            text={isInCart ? "Remove" : "Add"}
                                        />
                                        <Button
                                            func={() => setModalContent(null)}
                                            text={"Cancel"}
                                        />
                                    </div>
                                ),
                            })
                        }
                    />
                </div>
            </div>
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.exact({
        name: PropTypes.string.isRequired,
        price: PropTypes.number,
        imagePath: PropTypes.string,
        articul: PropTypes.string,
        color: PropTypes.string,
    }),
}

export default ProductCard
