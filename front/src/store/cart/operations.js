import actions from './actions'

const {toggleCart} = actions

const defForExport = {toggleCart}

export default defForExport