import React from "react"
import PropTypes from "prop-types"
import Icon from "../Icon"

import { useDispatch } from "react-redux"
import { productsOperations } from "../../store/products"

const FavoritesTableItem = ({ product }) => {
    const { articul, name, imagePath, price } = product

    const dispatch = useDispatch()

    return (
        <tr>
            <td>
                <button
                    className="favorite-delete-btn"
                    onClick={() =>
                        dispatch(productsOperations.toggleFavorites(articul))
                    }
                >
                    <Icon type="star" filled size={"medium"} />
                </button>
            </td>
            <td>
                <img width="200" src={imagePath} alt={name} />
            </td>
            <td>{name}</td>
            <td>{price}$</td>
        </tr>
    )
}

FavoritesTableItem.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        articul: PropTypes.string.isRequired,
        imagePath: PropTypes.string.isRequired,
        price: PropTypes.number,
    }).isRequired,
}

export default FavoritesTableItem
