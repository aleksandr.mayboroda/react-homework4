import actions from "./actions"
import axios from "axios"

const fetchProducts = () => (dispatch, getState) => {
    axios("/api/products")
        .then((response) => {
            dispatch(actions.setProducts(response.data))
        }, [])
        .catch((err) => {
            dispatch(actions.setError(err)) //не забывать про dispatch
        })
}

const { toggleFavorites } = actions

const defForExport = { fetchProducts, toggleFavorites }

export default defForExport
