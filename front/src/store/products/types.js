const SET_PRODUCTS = 'homework/products/SET_PRODUCTS'
const SET_ERROR = 'homework/products/SET_ERROR'
const TOGGLE_FAVORITES = 'homework/products/TOGGLE_FAVORITES'

const defForExport = {SET_PRODUCTS, SET_ERROR, TOGGLE_FAVORITES}

export default defForExport
