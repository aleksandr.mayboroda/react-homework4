import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { productsOperations, productsSelectors } from "../../store/products"

import "./style.scss"

import ProductCard from "../../components/ProductCard"
import EmptyEntity from "../../components/EmptyEntity"

const PageProducts = () => {
    const productList = useSelector(productsSelectors.getProducts())

    const dispatch = useDispatch()
    useEffect(() => dispatch(productsOperations.fetchProducts()), [dispatch])

    // const error = useSelector(productsSelectors.getError())//пока не будем выводить
    // if(error)
    // {
    //     return (
    //         <p style={{color: 'red', fontSize: '25px'}}>{error.toString()}</p>
    //     )
    // }

    return (
        <div className="product-page">
            {productList.length <= 0 && (
                <EmptyEntity>
                    <p>there is no products... yeat... i hope...</p>
                </EmptyEntity>
            )}
            {productList.length > 0 && (
                <div className="product-page__list">
                    {productList.map((product) => (
                        <ProductCard
                            key={product.articul}
                            product={product}
                        />
                    ))}
                </div>
            )}
        </div>
    )
}

export default PageProducts
