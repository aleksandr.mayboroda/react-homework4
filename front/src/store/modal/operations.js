import actions from './actions'

const {setModalContent} = actions

const defForExport = {setModalContent}

export default defForExport