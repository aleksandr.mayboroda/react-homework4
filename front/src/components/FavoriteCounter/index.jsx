import "./style.scss";
// import PropTypes from "prop-types";
import {useSelector} from 'react-redux'
import {productsSelectors} from '../../store/products'

import Icon from "../Icon";

const FavoriteCounter = () =>  //{ counter }
{
  const counter = useSelector(productsSelectors.countProductsInFavorites())

  return (
    <div className="favorite-list">
      <span className="favorite-list__text">
        You have <span className="favorite-list__counter">{counter}</span>{" "}
        {counter > 1 ? "products" : "product"} in favorites
      </span>
      <Icon type="star" filled={counter ? true : false} size={'medium'}/>
    </div>
  );
}

// FavoriteCounter.propTypes = {
//   counter: PropTypes.number.isRequired,
// };

export default FavoriteCounter;
