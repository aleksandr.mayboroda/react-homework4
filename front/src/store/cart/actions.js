import types from './types'

const toggleCart = (productObject) => 
({
    type: types.TOGGLE_CART,
    payload: productObject
})

const objForExport = {toggleCart}

export default objForExport