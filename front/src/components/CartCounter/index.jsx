// import PropTypes from "prop-types";
import "./style.scss";

import {useSelector} from 'react-redux'
import {cartSelectors} from '../../store/cart'

const CartCounter = () => 
{
  const counter = useSelector(cartSelectors.countProductsInCart())
  const sum = useSelector(cartSelectors.countSumProductsInCart())

  return (
    <div className="cart">
      <div className="cart__text">
        <span>
          You have <span className="cart__counter">{counter}</span>{" "}
          {counter > 1 ? "products" : "product"} in cart
        </span>
        {sum > 0 && (
          <>  
            for <span className="cart__sum"> {sum}$</span>
          </>
        )}
      </div>
      <img src="/cart.png" alt="cart__image" className="cart__image" />
    </div>
  );
}

// CartCounter.propTypes = {
//   counter: PropTypes.number.isRequired,
//   sum: PropTypes.number.isRequired,
// };

export default CartCounter;
