import React from 'react'

import ProductsTable from '../../components/ProductsTable'

function PageCart() {
    return (
        <ProductsTable />
    )
}

export default PageCart
