const getCart = () => (state) => state.cart.data

const isProductInCart = (articul) => (state) =>
    !!state.cart.data.find((product) => product.articul === articul)

const countProductsInCart = () => (state) =>
    state.cart.data.length > 0 ? state.cart.data.length : 0

const countSumProductsInCart = () => (state) =>
    state.cart.data.length > 0
        ? state.cart.data.reduce((acc, cur) => acc + cur.price, 0)
        : 0

const defForExport =  {
    getCart,
    isProductInCart,
    countProductsInCart,
    countSumProductsInCart,
}

export default defForExport
