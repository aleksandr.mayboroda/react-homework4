import types from './types'

const setProducts = (products) =>
({
    type: types.SET_PRODUCTS,
    payload: products
})

const setError = (error) => 
({
    type: types.SET_ERROR,
    payload: error
})

const toggleFavorites = (articul) =>
({
    type: types.TOGGLE_FAVORITES,
    payload: articul
})


const defForExport = {setProducts,setError,toggleFavorites}

export default defForExport